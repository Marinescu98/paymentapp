﻿using Microsoft.EntityFrameworkCore;
using Payment.Data;
using System;

namespace Payment.Repository
{
    public class PaymentDbContext:DbContext
    {
        public PaymentDbContext(DbContextOptions<PaymentDbContext> options):base(options)
        {

        }

        public DbSet<PaymentDetail> PaymentDetails { get; set; }
    }
}
