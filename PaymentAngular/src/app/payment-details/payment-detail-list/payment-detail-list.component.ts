import { Component, OnInit } from '@angular/core';
import { PaymentDetailService } from 'src/app/shared/payment-detail.service';
import { PaymentDetail } from 'src/app/shared/payment-detail.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-payment-detail-list',
  templateUrl: './payment-detail-list.component.html',
  styles: []
})
export class PaymentDetailListComponent implements OnInit {

  constructor(public service:PaymentDetailService,
    private toastr:ToastrService) { }

  ngOnInit(): void {
    this.service.refreshList();
  }
  
  populateForm(pd){
    //this.service.fromData=pd;
    this.service.getPaymentDetail(pd);
    console.log(this.service.updateFormData);
    this.service.fromData=Object.assign({},pd);
    
  }

  onDelete(pd){
    if(confirm("Are you sure to delete this record?")){
       this.service.deletePaymentDetail(pd).subscribe
    (res => {
      this.service.refreshList();
      this.toastr.warning("Deleted successfully!","Payment Resgister");
        },
      err=>{
        console.log(err);
      });
    }
  }
}
