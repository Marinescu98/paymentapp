import { Component, OnInit } from '@angular/core';
import { PaymentDetail } from 'src/app/shared/payment-detail.model';
import { PaymentDetailService } from 'src/app/shared/payment-detail.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-payment-detail',
  templateUrl: './payment-detail.component.html',
  styles: []
})
export class PaymentDetailComponent implements OnInit {

  constructor(public service: PaymentDetailService, private toaster: ToastrService) { }

  ngOnInit(): void {
    this.resetFrom();
  }

  resetFrom(form?: NgForm) {
    if (form != null)
      form.resetForm();

    this.service.fromData = {
      PMId: 0,
      CardOwnerName: "",
      CardNumber: '',
      ExpirationDate: '',
      CVV: ''
    };
  }

  onSubmit(form: NgForm) {
    if (form.value.PMId == 0) {
      this.insertRecord(form);
    } else {
      this.updateRecord(form);
    }

  }

  insertRecord(form: NgForm) {
    this.service.postPaymentDetail(form.value).subscribe(
      res => {
        this.resetFrom(form);
        this.toaster.info('Submitted successfully!', 'Payment Detail Register');
        this.service.refreshList();
      },
      err => {
        console.log(err);
      }

    );
  }


  updateRecord(form: NgForm) {
    if(confirm("Merge!")){
      this.service.putPaymentDetail(form.value).subscribe(
      res => {
        this.resetFrom(form);
        this.toaster.success('Submitted successfully!', 'Payment Detail Register');
      },
      err => {
        console.log(err);
      }

    );
    }
    
  }
}
