import { Injectable } from '@angular/core';
import { PaymentDetail } from './payment-detail.model';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})

export class PaymentDetailService {

  readonly rootURL = "https://localhost:44329/api";
  list:PaymentDetail[];
  updateFormData:PaymentDetail;
  fromData: PaymentDetail;
  constructor(private http: HttpClient) { }

  postPaymentDetail(fromData: PaymentDetail) {
    return this.http.post(this.rootURL + '/PaymentDetails', fromData);
  }

  putPaymentDetail(fromData: PaymentDetail) {
    return this.http.put(this.rootURL + '/PaymentDetails/'+fromData.PMId, fromData);
  }

  getPaymentDetail(pd) {
    this.http.get(this.rootURL + '/PaymentDetails/'+pd).toPromise().then(res => this.updateFormData as PaymentDetail);
  }

  deletePaymentDetail(pd) {
    return this.http.delete(this.rootURL + '/PaymentDetails/'+pd);
  }
  refreshList(){
    this.http.get(this.rootURL + '/PaymentDetails').toPromise().
    then(res => this.list = res as PaymentDetail[]);
  }
}
